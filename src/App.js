import React, {Component} from 'react';
import {componentWillMount} from 'react-native';
import {Provider} from 'react-redux';
import {createStore,applyMiddleware} from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import Router from './Router'

class App extends Component{
  componentWillMount(){
    const config = {
      apiKey: "AIzaSyAZENUOf2Qiej0gGoELPVMgqLrCNnDs1PE",
      authDomain: "manager-d4fd6.firebaseapp.com",
      databaseURL: "https://manager-d4fd6.firebaseio.com",
      projectId: "manager-d4fd6",
      storageBucket: "",
      messagingSenderId: "588189219137"
    };
    firebase.initializeApp(config);
  }
  render(){
    return(
      <Provider store={createStore(reducers, applyMiddleware(ReduxThunk))}>
          <Router/>
      </Provider>
    );
  }
}
export default App;
