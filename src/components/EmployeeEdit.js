import React, {Component} from 'react';
import _ from 'lodash';
import {View,Text,Picker,componentWillMount} from 'react-native'
import {Card,CardSection,Input,Button,Confirm} from './common'
import { employeeUpdate,employeeSave,deleteEmployee } from './../actions'
import {connect} from 'react-redux';
import EmployeeForm from './EmployeeForm';
import Communications from 'react-native-communications';

class EmployeeEdit extends Component{
state = {'showModal': false}

componentWillMount(){
  _.each(this.props.employee, (value,prop) => {
    this.props.employeeUpdate({prop,value});
  });
}
  onSaveButtonPress(){
    const {name,phone,shift} = this.props;
    this.props.employeeSave({name,phone,shift, uid: this.props.employee.uid})
  }
  onTextButtonPress(){
    const {name,phone,shift} = this.props;
    Communications.text(phone,`Hi ${name}, your upcoming shift is on ${shift}`);
  }
  onFireButtonPress(){
    this.setState({'showModal':true})
  }
  onFireDecline(){
    this.setState({'showModal':false})
  }
  onFireAccept(){
    this.props.deleteEmployee({uid: this.props.employee.uid});
  }
  render(){

    return(
      <Card>
        <EmployeeForm />
          <CardSection>
            <Button onPress={this.onSaveButtonPress.bind(this)}>
              Save Form
            </Button>
          </CardSection>
          <CardSection>
            <Button onPress={this.onTextButtonPress.bind(this)}>
              Text Schedule
            </Button>
            <Button onPress={this.onFireButtonPress.bind(this)}>
              Fire
            </Button>
          </CardSection>
          <Confirm
            visible={this.state.showModal}
            onDecline={this.onFireDecline.bind(this)}
            onAccept={this.onFireAccept.bind(this)}
            >
          Are you sure you want to delete this?
          </Confirm>
      </Card>
    );
  }
}


const mapStateToProps = (state) => {
  const {name,phone,shift} = state.employeeForm;
  return {name,phone,shift};
};

export default connect(mapStateToProps,{employeeUpdate,employeeSave,deleteEmployee})(EmployeeEdit);
