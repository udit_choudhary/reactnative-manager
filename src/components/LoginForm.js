import React, {Component} from 'react';
import {Card,CardSection,Input,Button,Spinner} from './common';
import {connect} from 'react-redux';
import * as actions from './../actions'
import {Text} from 'react-native';

class LoginForm extends Component{

onEmailChange(text){
  this.props.emailChanged(text);
}
onPasswordChange(text){
  this.props.passwordChanged(text);
}
onButtonPress(){
  const {email,password} = this.props;
  this.props.loginUser({email,password});
}

renderLoginButton(){
  if (this.props.loading) {
    return <Spinner />
  }

  return(
      <Button onPress={this.onButtonPress.bind(this)}>
        Login
      </Button>
  )
}
  render(){

    return(
      <Card>
        <CardSection>
          <Input
            label='Email'
            placeHolder='Enter email address'
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </CardSection>

        <CardSection>
        <Input
          secureTextEntry
          label='Password'
          placeHolder="Enter password"
          onChangeText={this.onPasswordChange.bind(this)}
          value={this.props.password}
        />
        </CardSection>
          <Text>{this.props.error}</Text>
          <CardSection>
              {this.renderLoginButton()}
          </CardSection>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    email: state.auth.email,
password: state.auth.password,
error: state.auth.error,
loading: state.auth.loading
   }
}

export default connect(mapStateToProps,actions)(LoginForm);
