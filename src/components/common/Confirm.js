import React from 'react';
import { Text, View, Modal } from 'react-native';
import {CardSection} from './CardSection';
import {Button} from './Button';

const Confirm = ({children, onAccept, onDecline, visible}) => {
const {containerStyle,cardSectionStyle,textStyle} = styles;
  return(
    <Modal
    animationType="slide"
    onRequestClose={()=>{}}
    transparent={true}
    visible={visible}

    >
      <View style={containerStyle}>
        <CardSection style={cardSectionStyle}>
          <Text style={textStyle}>{children}</Text>
        </CardSection>

        <CardSection style={cardSectionStyle}>
          <Button onPress={onAccept}>Yes</Button>
          <Button onPress={onDecline}>No</Button>
        </CardSection>
      </View>
    </Modal>
  );

};

const styles = {
  containerStyle:{

    backgroundColor:'rgba(0,0,0,0.75)',
    flex:1,
    position:'relative',
    justifyContent:'center',


  }
,
cardSectionStyle:{
justifyContent:'center'
},
textStyle:{
  flex:1,
  fontSize: 18,
  textAlign: 'center',
  lineHeight: 40
}
}

export {Confirm};
