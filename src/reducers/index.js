import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer'
import EmployeeCreateReducer from './EmployeeCreateReducer';
import EmployeeFetchReducer from './EmployeeReducer';

export default combineReducers({
  auth: AuthReducer,
  employeeForm:EmployeeCreateReducer,
  employees:EmployeeFetchReducer
});
